
import flask
from flask import Flask,request,Response,render_template,make_response
from flask_restful import Resource, Api
import pymongo
import csv
import json
from pymongo import MongoClient
from password import hash_password, verify_password
from testToken import generate_auth_token, verify_auth_token
from flask_login import LoginManager,login_user,logout_user,login_required
from bson.objectid import ObjectId
from flask_wtf import FlaskForm
from wtforms import Form, StringField,PasswordField, BooleanField,validators
from wtforms.validators import InputRequired

app = Flask(__name__)
api = Api(app)

app.config["SECRET_KEY"] = "123456"
client = MongoClient('db', 27017)
db = client.tododb
collection = db.control

login_manager = LoginManager()
login_manager.init_app(app)

class User():
	def __init__(self, user_id):
		self.user_id = user_id

	def is_authenticated(self):
		return True
	def is_active(self):
		return True
	def is_anonymous(self):
		return False
	def get_id(self):
		return self.user_id


class LoginForm(FlaskForm):
	username = StringField('username', validators=[InputRequired()])
	password = PasswordField('password', validators=[InputRequired()])
	remember = BooleanField('remember me')

class RegisterForm(FlaskForm):
	username = StringField('username', validators=[InputRequired()])
	password = PasswordField('password', validators=[InputRequired()])

@login_manager.user_loader
def load_user(user_id):
	ID = str(user_id)
	user = collection.users.find_one(ObjectId(ID))
	app.logger.debug(user)
	if user == None:
		return None
	return User(user['_id'])

@app.route("/api/register", methods = ["GET", "POST"])
def register():
	form = RegisterForm(request.form)
	if form.validate_on_submit(): 
		username = form.username.data
		password = form.password.data
		if collection.users.find_one({"username":username}) != None:
			return render_template("400.html"), 400
		if username == None or password == None:
			title = "Register an Account"
			return render_template('register.html', form=form, title=title)
		hashed_pass = hash_password(password)
		id_ = collection.users.insert_one({"username":username,"password":hashed_pass})
		user = {"username":username,"password":password}
		password = None
		return render_template("success.html",user=json.dumps(user),location=str(id_.inserted_id)),201
	title = "Register an account"
	return render_template("register.html", form=form, title=title)

@app.route("/api/token", methods=["GET", "POST"])
def login():
	form = LoginForm(request.form)
	username = form.username.data
	password = form.password.data
	remember = form.remember.data
	if request.method == 'POST' and form.validate_on_submit():
		user = collection.users.find_one({"username":username})     
		if user == None:
			title = "Please create an account first"
			return render_template('register.html', form=form, title=title)
		if not verify_password(password, user['password']):
			return render_template("401.html"), 401
		userID = str(user['_id'])
		user_obj = User(userID)
		login_user(user_obj, remember=remember)
		token = generate_auth_token(expiration=1000)
		return flask.jsonify({'token':token.decode(),'duration':1000}), 200
	return render_template('login.html', form=form)


@app.route("/logout")
@login_required
def logout():
	logout_user()
	return render_template('logout.html')

class All(Resource):
	@login_required
	def get(self):
		token = request.args.get("token")
		if token == None:
			headers = {'Content-Type': 'text/html'}
			return make_response(render_template("401.html"),401,headers)
		valid = verify_auth_token(token)
		if valid == True:
			n = request.args.get('top')
			app.logger.debug('n equals ',n)
			if n == None:
				top = 20
			else:
				top = int(n)
			times = collection.find().sort([("open_time",pymongo.ASCENDING), ("close_time",pymongo.ASCENDING)]).limit(100)
			times_list = []
			for time in times:
				if time['open_time'] != '' and time['close_time'] != '':
					times_list.append((time['open_time'],time['close_time']))
			times_list = times_list[:top]
			app.logger.debug(times_list)
			out = []
			for open_t, close_t in times_list:
				out.append({'open':open_t,'close':close_t})
			return flask.jsonify(result= out)
		else:
			headers = {'Content-Type': 'text/html'}
			return make_response(render_template("401.html"),401,headers)
api.add_resource(All, "/listAll",'/listAll/json')


class Open_only(Resource):
	def get(self):
		token = request.args.get("token")
		if token == None:
			headers = {'Content-Type': 'text/html'}
			return make_response(render_template("401.html"),401,headers)
		valid = verify_auth_token(token)
		if valid == True:
			n = request.args.get('top')
			if n == None:
				top = 20
			else:
			    top = int(n)
			times = collection.find().sort("open_time",pymongo.ASCENDING).limit(100)
			times_list = []
			for time in times:
				if time['open_time'] != '':
					times_list.append(time['open_time'])
			times_list = times_list[:top]
			times = []
			for open_t in times_list:
			    times.append({'open':open_t})
			return flask.jsonify(result= times)
		else:
			headers = {'Content-Type': 'text/html'}
			return make_response(render_template("401.html"),401,headers)
api.add_resource(Open_only, '/listOpenOnly', "/listOpenOnly/json")

class Close_only(Resource):
	def get(self):
		token = request.args.get("token")
		if token == None:
			headers = {'Content-Type': 'text/html'}
			return make_response(render_template("401.html"),401,headers)
		valid = verify_auth_token(token)
		if valid == True:
			n = request.args.get('top')
			if n == None:
				top = 20
			else:
				top = int(n)
			times = collection.find().sort("close_time",pymongo.ASCENDING).limit(100)
			times_list = []
			for time in times:
				if time['close_time'] != '':
					times_list.append(time['close_time'])
			times_list = times_list[:top]
			times = []
			for close_t in times_list:
				times.append({'close':close_t})
			return flask.jsonify(result= times)
		else:
			headers = {'Content-Type': 'text/html'}
			return make_response(render_template("401.html"),401,headers)
api.add_resource(Close_only, '/listCloseOnly', "/listCloseOnly/json")

class All_csv(Resource):
	def get(self):
		token = request.args.get("token")
		if token == None:
			headers = {'Content-Type': 'text/html'}
			return make_response(render_template("401.html"),401,headers)
		valid = verify_auth_token(token)
		if valid == True:
			n = request.args.get('top')
			if n == None:
				top = 20
			else:
				top = int(n)
			times = collection.find().sort([("open_time",pymongo.ASCENDING), ("close_time",pymongo.ASCENDING)]).limit(100)
			times_list = []
			for time in times:
				if time['open_time'] != '' and time['close_time'] != '':
					times_list.append((time['open_time'],time['close_time']))
			times_list = times_list[:top]
			csv = open('data.csv', "w")
			csv.write("Open Time,Close Time\n") 
			for open_t,close_t in times_list:
				row = open_t + "," + close_t + "\n"
				csv.write(row)
			csv = open('data.csv', "r")
			return Response(csv, mimetype='text/csv')
		else:
			headers = {'Content-Type': 'text/html'}
			return make_response(render_template("401.html"),401,headers)  
api.add_resource(All_csv, "/listAll/csv")

class Open_only_csv(Resource):
	def get(self):
		token = request.args.get("token")
		if token == None:
			headers = {'Content-Type': 'text/html'}
			return make_response(render_template("401.html"),401,headers)
		valid = verify_auth_token(token)
		if valid == True:
			n = request.args.get('top')
			if n == None:
				top = 20
			else:
				top = int(n)
			times = collection.find().sort("open_time",pymongo.ASCENDING).limit(100)
			times_list = []
			for time in times:
				if time['open_time'] != '':
					times_list.append(time['open_time'])     
			times_list = times_list[:top]
			csv = open('data.csv', "w")
			csv.write("Open Time\n")
			for open_t in times_list:
				row = open_t + "\n"
				csv.write(row)
			csv = open('data.csv', "r")
			return Response(csv, mimetype='text/csv')
		else:
			headers = {'Content-Type': 'text/html'}
			return make_response(render_template("401.html"),401,headers)
api.add_resource(Open_only_csv, "/listOpenOnly/csv")

class Close_only_csv(Resource):
	def get(self):
		token = request.args.get("token")
		if token == None:
			headers = {'Content-Type': 'text/html'}
			return make_response(render_template("401.html"),401,headers)
		valid = verify_auth_token(token)
		if valid == True:
			n = request.args.get('top')
			if n == None:
				top = 20
			else:
				top = int(n)
			times = collection.find().sort("close_time",pymongo.ASCENDING).limit(100)
			times_list = []
			for time in times:
				if time['close_time'] != '':
					times_list.append(time['close_time']) 
			times_list = times_list[:top]           
			csv = open('data.csv', "w")
			csv.write("Close Time\n")
			for close_t in times_list:
				row = close_t + "\n"
				csv.write(row)
			csv = open('data.csv', "r")
			return Response(csv, mimetype='text/csv')
		else:
			headers = {'Content-Type': 'text/html'}
			return make_response(render_template("401.html"),401,headers)
api.add_resource(Close_only_csv, "/listCloseOnly/csv")   

if __name__ == '__main__':
    app.run(debug=True, port=80, host='0.0.0.0')