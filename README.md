# Brevet time calculator with Ajax

## Created by Zac Sims, zsims@uoregon.edu

This is a system to calculate open and close times for control points along standard ACP-sactioned brevets of length 200km, 300km, 400km, 600km and 1000km. The calculation for these times is as outlined by Randonneurs USA (RUSA). A description of this calculation can be found here: https://rusa.org/pages/acp-brevet-control-times-calculator. (Note: A varation of the USA algorithm is being used here, not the French version.

Considerations: As the longest ACP-sanctioned brevet is 100km, a maximum distance for any control point is 1000km. If a control point is specificed at the brevet start, the open and close times for that control point will be the start time of the brevet, and one hour after the start time, per RUSA guidelines.

### API

The calculator is located at http://localhost:5002

* RESTful service to expose what is stored in MongoDB:
    * "http://localhost:5001/listAll" should return all open and close times in the database
    * "http://localhost:5001/listOpenOnly" should return open times only
    * "http://localhost:5001/listCloseOnly" should return close times only


* A user must be registered and logged in in order to access API features. "http://localhost:5001/api/register" will allow you to do that.

* To access an API resource, you must obtain a token by logging in at
"http://localhost:5001/api/token" copy the token, and access the resource by adding it to the end of the address like so: "http://localhost:5001/<resource>?=<token>

* You may logout by accessing http://localhost:5001/logout

* There are two different datta representations: csv and json. JSON is the default representation for the above three APIs. 
    * "http://localhost:5001/listAll/csv" should return all open and close times in CSV format
    * "http://localhost:5001/listOpenOnly/csv" should return open times only in CSV format
    * "http://localhost:5001/listCloseOnly/csv" should return close times only in CSV format

    * "http://localhost:5001/listAll/json" should return all open and close times in JSON format
    * "http://localhost:5001/listOpenOnly/json" should return open times only in JSON format
    * "http://localhost:5001/listCloseOnly/json" should return close times only in JSON format

* There is a query parameter to get top "k" open and close times. For examples, see below.
    * "http://localhost:5001/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://localhost:5001/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://localhost:5001/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://localhost:5001/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

